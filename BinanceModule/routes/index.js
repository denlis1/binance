import { Router } from 'express'
const router = Router();
import config from '../config'
const Web3 = require('web3')
import mysqlPromise from 'mysql2/promise'


const WalletFactory = require('../lib/wallet').WalletFactory
let _walletFactory = new WalletFactory(config.wallet.mnemonics, config.wallet.password, config.wallet.network)


const pool = mysqlPromise.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
//  port: config.db.port,
  database: config.db.dbName,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
})

/* GET home page. */
router.get('/depositAddress', async (req, res, next) => {
  try {
    let { userId } = req.query
    let [rows,fields] = await pool.query("SELECT address from bnbAddresses WHERE `ref` = ?", [userId])
	let result
//    console.log(rows[0].address)
    if(rows.length == 0){
       //let { address } = await tronLib.getAccountAtIndex(userId)
       let address  = await getAccountAtIndex(userId)
       console.log("address",address)
       
       await pool.query('INSERT IGNORE INTO bnbAddresses (address, addressHex, ref) VALUES (?, ?, ?)', [address,address, userId])
	result = address
	
    } else { result = rows[0].address}

    res.send({
      status: true,
      address: result
    })

  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: false,
      message: 'Error occurred'
    })
  }
})

router.post('/send', async (req, res, next) => {
  try {
    let { receiver, amount } = req.body

    //let receipt = await tronLib.sendTrx(receiver, Number(amount) * 10 ** 6)
    await sendTrx(receiver, Number(amount),res)
    
    
  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: false,
      message: 'Error occurred'
    })
  }
})


router.use((err, req, res, next) => {
  res.status(400).send({
    status: false,
    message: err.message
  })
})

async function getAccountAtIndex(userId) {
  const web3 = new Web3()
  try {
     
      let ref = Number(userId)
      
      let isTestnet = config.wallet.eth.network == 'testnet' ? true : false
      let key = await _walletFactory.getExtendedKey(ref, isTestnet);
      let normalAddress = await _walletFactory.generateEthereumWallet(key);
    
      let address = web3.utils.toChecksumAddress(normalAddress.address)
      console.log("address", address)
      return address
  } catch (error) {
      console.log(error)
  }

}

async function sendTrx(receiver,amount,res) {
  const web3 = new Web3(new Web3.providers.HttpProvider(config.wallet.provider))
    let wallet = web3.eth.accounts.wallet 
    wallet.clear()
    wallet = wallet.create(0)
    wallet.add(config.wallet.eth.privKey)
    console.log("wallet[0]",wallet[0])
    try {
      let gasPriceTx = await web3.eth.getGasPrice()
      console.log({ gasPrice: gasPriceTx})
      await web3.eth.sendTransaction({
        from: wallet[0],
        to: await web3.utils.toChecksumAddress(receiver),
        value: web3.utils.toWei(String(amount)),
        nonce: await web3.eth.getTransactionCount(wallet[0].address, "pending"),
        gasPrice: gasPriceTx, //'0x1DCD65000',
        gasLimit: "0x5208"//config.wallet.gasLimit

      }).on('transactionHash', async hash => {
        console.log("hash",hash)
        let txRec = await web3.eth.getTransaction(hash)
        console.log("txRec",txRec)
        await pool.query('INSERT INTO bnbTransactions (txid, status, toAddress, amount, type) VALUES (?, ?, ?, ?, ?)', [
          hash, 'pending', receiver, amount,  'withdraw'
        ])
        res.send({
          status: true,
          message: 'Transaction Initiated',
          hash: hash
        })
        
      })
    } catch (err) {
      console.log(err.message)
      res.status(400).send({
        status: false,
        message: err.message
      })
      
    }
}

export default router;
