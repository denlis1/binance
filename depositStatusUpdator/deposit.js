
import config from './config'
import mysqlPromise from 'mysql2/promise'
const Web3 = require('web3')
const web3 = new Web3(new Web3.providers.HttpProvider(config.wallet.provider))
require('array-foreach-async')

const pool = mysqlPromise.createPool({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
  //  port: config.db.port,
    database: config.db.dbName,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
  })

const getAllUsers = async () => {
    let userAddresses = await pool.query("SELECT addressHex FROM bnbAddresses")
    return userAddresses
}

let blockNumber = config.blockNumber
async function main() {
    try {
        let users = await getAllUsers()
        try {
            let transactions = await web3.eth.getBlock(String(blockNumber),true)
            console.log('Checking Block', blockNumber)
            blockNumber = blockNumber + 1
            
            await transactions.transactions.forEachAsync(async _tx => {
                if (_tx.to) {
                    for (let i = 0; i < users[0].length; i++) {
                        try {
                            if (web3.utils.toChecksumAddress(_tx.to) == users[0][i].addressHex) {
                                let receipt = await web3.eth.getTransactionReceipt(_tx.hash)
                                console.log('receipt', receipt)
                                _tx.value = _tx.value/(10**18)
                                await pool.query('INSERT INTO bnbTransactions (txid, status, toAddress, amount, result, type) VALUES (?, ?, ?, ?, ?, ?)', [
                                    _tx.hash, 'waiting_for_confirmation', _tx.to, _tx.value, receipt.status, 'deposit'
                                ])
                            }
                        } catch { }

                    }
                }

            })
            await sleep(2000)
            setImmediate(main)
        } catch (error) {
            console.log(error)
            await sleep(2000)
            setImmediate(main)
           
        }

    } catch (error) {
        console.log(error)
    }
}
const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}


main().then(console.log)
